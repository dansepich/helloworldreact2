import React, { Component } from 'react';

class weather extends React.Component {
   callApi(){
      // Github fetch library : https://github.com/github/fetch
      // Call the API page
      fetch('https://api.openweathermap.org/data/2.5/weather?zip=50007&appid=c221bd6c9648d81b1449f3fafca91a12&units=imperial')
      .then((result) => {
        // Get the result
        // If we want text, call result.text()
        return result.json();
      }).then((jsonResult) => {
        // Do something with the result
        console.log(jsonResult);
      })
    }
  }
  
//React.render(<weather />, document.getElementById('app'));
export default weather;